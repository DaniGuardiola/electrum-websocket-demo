const WebSocketModule = global.WebSocket || require('ws')
const JsonRpc = require('simple-jsonrpc-js')

module.exports = async (type, host, port, tls) => {
  let readyState = false

  // configure
  const jrpc = new JsonRpc()
  const socket = new WebSocketModule(`ws${tls ? 's' : ''}://${host}:${port}`)

  // wait of call
  jrpc.on('view.setTitle', function (title) {
    document.getElementsByClassName('title')[0].innerHTML = title
  })

  socket.onmessage = event => jrpc.messageHandler(event.data)

  jrpc.toStream = msg => socket.send(msg)

  socket.onerror = error => console.error('Error: ' + error.message)

  socket.onclose = event => {
    if (event.wasClean) console.info('Connection close was clean')
    else console.error('Connection suddenly close')

    console.info(`Close code: ${event.code} reason: ${event.reason}`)
  }

  const ready = new Promise(resolve => {
    if (readyState) resolve()
    socket.onopen = () => {
      readyState = true
      resolve()
    }
  })

  await ready

  return jrpc
}
