
// ----------------
// modules

const commandLineArgs = require('command-line-args')

const getRPC = require('./rpc')

// ----------------
// command line arguments

const DEFAULT_TYPE = 'websocket'

const optionDefinitions = [
  { name: 'host', alias: 'h', type: String },
  { name: 'port', alias: 'p', type: Number },
  { name: 'type', alias: 't', type: String },
  { name: 'tls', alias: 's', type: Boolean }
]

const options = commandLineArgs(optionDefinitions)

options.type = options.type || DEFAULT_TYPE

if (!options.host) throw new Error('❌   The --host (-h) option is required!')
if (!options.port) throw new Error('❌   The --port (-p) option is required!')
if (options.type !== 'websocket') throw new Error(`❌   The --type (-t) option supports only 'websocket' at the moment!`)
if (options.tls) throw new Error(`❌   The --tls (-s) option is not yet ready!`)

// ----------------
// websocket

const run = async () => {
  const rpc = await getRPC(options.type, options.host, options.port, options.tls)

  const serverVersion = await rpc.call('server.version', [])
  console.log(`JSON-RPC response | server.version: ${serverVersion}`)

  const balance = await rpc.call('blockchain.address.get_history', ['3D2oetdNuZUqQHPJmcMDDHYoqkyNVsFk9r'])
  console.log(`JSON-RPC response | blockchain.address.get_history (3D2oetdNuZUqQHPJmcMDDHYoqkyNVsFk9r): ${balance}`)

  const batch = await rpc.batch([
    {call: {method: 'server.version', params: []}},
    {call: {method: 'blockchain.address.get_history', params: ['3D2oetdNuZUqQHPJmcMDDHYoqkyNVsFk9r']}},
    {call: {method: 'server.version', params: []}}
  ])
  console.log(batch)
}

run()
