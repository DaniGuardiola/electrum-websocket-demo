# ElectrumX Server WebSocket Support

This document is intended as a place to gather all the knowledge related to the integration of the websocket protocol into electrumx.

> [Electrumx repo](https://github.com/kyuupichan/electrumx/)

> [Websocket integration issue](https://github.com/kyuupichan/electrumx/issues/499)


# Demo / testing

This repo contains a demo of a connection to an electrumx server through websocket, with simple commands and no validation (yet!).

## Environment setup for demo

- Install NodeJS v8+.

The easiest and more convenient way is installing [nvm (Node Version Manager)](http://nvm.sh) and then using it to install NodeJS v8.

- Clone and prepare this repo

```bash
# clone project locally
git clone <this project>

# enter the repo dir
cd electrum-websocket-demo

# install node modules
npm i
```

## Execution

In the repo dir, run the following command with your electrumx server details:

```
npm run start -h <host> -p <port> [-s]
```

> `-s` means TLS support (not set up yet)

For now, as there's no validation, the only output will be the responses of the server to the commands as logs. Checking the code (main.js) is recommended to see what's going on.

# Information

## Goal

Add support for WebSocket in ElectrumX server, with TLS too.

## Background

Electrum is a widely known lightweight bitcoin wallet for desktop, meaning that the client doesn't have the whole blockchain stored and instead makes and verifies enquiries to the server.

The channel the client and server communicate through is a TCP socket (with or without TLS)

The protocol they use to understand each other is [JSON-RPC 2.0](https://electrumx.readthedocs.io/en/latest/protocol-basics.html).

The client uses [methods](https://electrumx.readthedocs.io/en/latest/protocol-methods.html) to request some data, and the server answers back using the same JSON-RPC id.

It can also listen for notifications triggered by using subscription methods.

## Technical analysis

> About [electrumx](https://github.com/kyuupichan/electrumx)

> [Issue on electrumx repo](https://github.com/kyuupichan/electrumx/issues/499)

- Written in Python
- Modules in use:
  - [asyncio](https://docs.python.org/3/library/asyncio.html)
  - [aiorpcx](https://aiorpcx.readthedocs.io/en/latest/socks.html) ([`Server` class](https://aiorpcx.readthedocs.io/en/latest/session.html))
- Maintainer points to the use of `self.loop` (`asyncio.get_event_loop()`) `.create_server()` in [`server/controller.py`](https://github.com/kyuupichan/electrumx/blob/master/server/controller.py#L250)
- Maintainer [suggests](https://github.com/kyuupichan/electrumx/issues/499#issuecomment-398513511) that it might be better integrated into / with the [aiorpcx](https://github.com/kyuupichan/aiorpcX) package maintained by himself ([or maybe not](https://github.com/kyuupichan/electrumx/issues/499#issuecomment-401374806))
- Potentially useful resources:
  - [jsonrpc-websocket](https://github.com/armills/jsonrpc-websocket): compact JSON-RPC websocket client library for asyncio (no TLS?)
  - [websockets](https://websockets.readthedocs.io/en/stable/intro.html#secure-example): library for building WebSocket servers in Python

## Criteria for the PR acceptance (from [the maintainer's comments](https://github.com/kyuupichan/electrumx/issues/499#issuecomment-398329345) and common sense)

- Quality code
- Good documentation
- With tests